//
// Created by nylon on 16.10.2023.
//

#ifndef IMAGE_TRANSFORMER_VIEW_H
#define IMAGE_TRANSFORMER_VIEW_H
#include "controllers/io_status.h"
#include "controllers/parse.h"
#include "processors/image.h"
#include "processors/rotator.h"

#include <stdlib.h>

int rotate(int argc, char **argv);
int read_response(enum read_status status);
int write_response(enum write_status status);

#endif // IMAGE_TRANSFORMER_VIEW_H
