//
// Created by nylon on 16.10.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_MAPPER_H
#define IMAGE_TRANSFORMER_BMP_MAPPER_H
#include "bmp_header.h"
#include "io_status.h"
#include "processors/image.h"
#include <stdio.h>

enum read_status parse_input(const int argc, char **restrict argv,
                             int16_t *ang);
enum read_status from_bmp(FILE *file, struct image *img);
enum write_status to_bmp(FILE *file, struct image const *img);
enum read_status open_file(char const *restrict filename,
                           char const *restrict mode, FILE **file);

#endif // IMAGE_TRANSFORMER_BMP_MAPPER_H
